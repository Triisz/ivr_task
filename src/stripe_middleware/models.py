from django.core.validators import MinLengthValidator, RegexValidator
from django.db import models


# Create your models here.

class TransactionRequest(models.Model):
    cc_numb = models.CharField(validators=[MinLengthValidator(16), RegexValidator(regex=r"^[0-9]{16}$")],
                               max_length=16)
    cvc = models.CharField(validators=[MinLengthValidator(3), RegexValidator(regex=r"^[0-9]{3,4}$")], max_length=4)
    exp_date = models.CharField(validators=[MinLengthValidator(4), RegexValidator(regex=r"^[0-9]{4}$")], max_length=4)
    trans_id = models.CharField(max_length=60)

    def __str__(self):
        return self.cc_numb


class ClientCard(models.Model):
    object = models.CharField(max_length=60)
    brand = models.CharField(max_length=60)
    country = models.CharField(max_length=60)
    exp_month = models.CharField(validators=[RegexValidator(regex=r"^[0-9]{2}$")], max_length=2)
    exp_year = models.CharField(validators=[MinLengthValidator(4), RegexValidator(regex=r"^[0-9]{4}$")], max_length=4)
    cvc_check = models.CharField(max_length=60)
    funding = models.CharField(max_length=60)
    last4 = models.CharField(max_length=4)

    def __str__(self):
        return "Card %s" % self.last4


class TransactionResponse(models.Model):
    transaction_id = models.OneToOneField(TransactionRequest, on_delete=models.CASCADE)
    object = models.CharField(max_length=60)
    client_card = models.ForeignKey(ClientCard, on_delete=models.CASCADE)
    created = models.CharField(max_length=100)
    livemode = models.BooleanField(default=False)
    type = models.CharField(max_length=60)
    used = models.BooleanField(default=False)

    def __str__(self):
        return "Type %s - created %s" % (self.type, self.created)

