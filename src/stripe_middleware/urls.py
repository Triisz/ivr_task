from django.urls import path
from django.views.decorators.csrf import csrf_exempt

from stripe_middleware.views import StripeRequest

urlpatterns = [
    path('send/', csrf_exempt(StripeRequest.as_view()), name='send'),
]