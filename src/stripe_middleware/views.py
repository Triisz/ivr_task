from rest_framework import generics
from rest_framework.response import Response

from stripe_middleware.serializers import RequestSerializer


class StripeRequest(generics.GenericAPIView):
    serializer_class = RequestSerializer
    def post(self, request, *args, **kwargs):
        """
        Send a POST REQUEST and return the result of the transaction (handles the middleware).
        :param request: Contains the middleware response
        :return: Return the middleware result: error, exception or success.
        """
        if hasattr(request, 'transaction_error'):
            return Response(request.transaction_error)

        if hasattr(request, 'status'):
            return Response(request.status['error'], request.status['status'])

        return Response('Success transaction, you can check the objects in django admin view')



