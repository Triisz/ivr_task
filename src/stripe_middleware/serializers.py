from rest_framework import serializers

from stripe_middleware.models import TransactionRequest, TransactionResponse, ClientCard


class RequestSerializer(serializers.ModelSerializer):
    class Meta:
        model = TransactionRequest
        exclude = ('id',)

    def create(self, validated_data):
        """
        Creates a TransactionRequest object with the cc numb mask and the cvc mask.
        :param validated_data: The directory that contains the object data.
        :return: The TransactionRequest object that was created.
        """

        _cc_numb = validated_data['cc_numb']
        _cvc = validated_data['cvc']
        cc_numb_mask = _cc_numb[-4:].rjust(len(_cc_numb), "*")
        cvc_mask = '*' * len(_cvc)

        new_request = TransactionRequest.objects.create(cc_numb=cc_numb_mask,
                                                        cvc=cvc_mask,
                                                        exp_date=validated_data['exp_date'],
                                                        trans_id=validated_data['trans_id']
                                                        )
        return new_request


class CardSerializer(serializers.ModelSerializer):
    class Meta:
        model = ClientCard
        fields = '__all__'


class ResponseSerializer(serializers.ModelSerializer):
    class Meta:
        model = TransactionResponse
        exclude = ('id', 'transaction_id', 'client_card')

    def save(self, transaction, card):
        """
        Saves a TransactionResponse object with the corrects FK to ClientCard and TransactionRequest.
        :param transaction: TransactionRequest object
        :param card: ClientCard object
        :return: The TransactionResponse object that was created.
        """
        credit_card = TransactionResponse.objects.create(transaction_id=transaction,
                                                         object=self.validated_data['object'],
                                                         client_card=card,
                                                         created=self.validated_data['created'],
                                                         livemode=self.validated_data['livemode'],
                                                         type=self.validated_data['type'],
                                                         used=self.validated_data['used']
                                                         )

        return credit_card
