import json
import logging

import stripe
from django.conf import settings
from django.urls import resolve, reverse
from rest_framework.status import HTTP_500_INTERNAL_SERVER_ERROR

from stripe_middleware.serializers import RequestSerializer, CardSerializer, ResponseSerializer


logger = logging.getLogger(__name__)
stripe.api_key = settings.STRIPE_PUB_KEY


class SimpleMiddleware:
    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):

        """
        Handles the entire logical procedure to show the result of a POST REQUEST
        It works only with a specific view.
        :param request: Contains the transaction information
        :return: Return the request with the necessary information to show
        """

        _current_url = resolve(request.path_info).route
        _post_url = reverse('send').lstrip('/')

        if _current_url == _post_url and request.method == "POST":

            try:
                if request.POST:
                    request_transaction = RequestSerializer(data=request.POST)
                else:
                    request_data = json.loads(request.body.decode())
                    request_transaction = RequestSerializer(data=request_data)

                if request_transaction.is_valid():

                    response_data = stripe.Token.create(
                        card={
                            "number": request_transaction.validated_data['cc_numb'],
                            "exp_month": request_transaction.validated_data['exp_date'][:2],
                            "exp_year": '20' + request_transaction.validated_data['exp_date'][2:],
                            "cvc": request_transaction.validated_data['cvc'],
                        },
                    )

                    logger.info('Stripe endpoint connection with Token.')

                    card_data = {'id': response_data['card']['id'],
                                 'object': response_data['card']['object'],
                                 'brand': response_data['card']['brand'],
                                 'country': response_data['card']['country'],
                                 'cvc_check': response_data['card']['cvc_check'],
                                 'exp_month': response_data['card']['exp_month'],
                                 'exp_year': response_data['card']['exp_year'],
                                 'funding': response_data['card']['funding'],
                                 'last4': response_data['card']['last4'],
                                 }

                    logger.info('Client card info generated')

                    card_instance = CardSerializer(data=card_data)

                    if card_instance.is_valid():

                        response_transaction_data = {'object': response_data['object'],
                                                     'created': response_data['created'],
                                                     'livemode': response_data['livemode'],
                                                     'type': response_data['type'],
                                                     'used': response_data['used'],
                                                     }

                        logger.info('Response transaction info generated')

                        response_transaction_instance = ResponseSerializer(data=response_transaction_data)

                        if response_transaction_instance.is_valid():
                            request_obj = request_transaction.save()
                            card_obj = card_instance.save()
                            response_transaction_obj = response_transaction_instance.save(request_obj,
                                                                                          card_obj
                                                                                          )

                            logger.info('Request object saved')
                            logger.info('Client card object saved')
                            logger.info('Response object saved')

                            setattr(request, "request_transaction", request_obj)
                            setattr(request, "client_card", card_obj)
                            setattr(request, "response_transaction", response_transaction_obj)

                            logger.warning('Request, Client card and Response objects added to middleware response '
                                           'object.')

                        else:
                            logger.error('Client card error:' + str(repr(response_transaction_instance.errors)))
                            setattr(request, 'transaction_error', response_transaction_instance.errors)

                    else:
                        logger.error('Client card error:' + str(repr(card_instance.errors)))
                        setattr(request, 'transaction_error', card_instance.errors)
                else:
                    logger.error('Request transaction error:' + str(repr(request_transaction.errors)))
                    setattr(request, 'transaction_error', request_transaction.errors)

            except stripe.error.InvalidRequestError as e:
                logger.exception('Stripe exception:' + str(e))
                context = {"error": str(e),
                           "status": e.http_status
                           }
                setattr(request, 'status', context)

            except stripe.error.AuthenticationError as e:
                logger.exception('Stripe exception:' + str(e))
                context = {"error": str(e),
                           "status": e.http_status
                           }
                setattr(request, 'status', context)

            except stripe.error.APIConnectionError as e:
                logger.exception('Stripe exception:' + str(e))
                context = {"error": str(e),
                           "status": e.http_status
                           }
                setattr(request, 'status', context)

            except stripe.error.StripeError as e:
                logger.exception('Stripe exception:' + str(e))
                context = {"error": str(e),
                           "status": e.http_status
                           }
                setattr(request, 'status', context)

            except stripe.error.RateLimitError as e:
                logger.exception('Stripe exception:' + str(e))
                context = {"error": str(e),
                           "status": e.http_status
                           }
                setattr(request, 'status', context)

            except Exception as e:
                logger.exception('Exception:' + str(e))
                context = {"error": str(e),
                           "status": HTTP_500_INTERNAL_SERVER_ERROR
                           }
                setattr(request, 'status', context)

        response = self.get_response(request)
        return response
