from django.apps import AppConfig


class StripeMiddlewereConfig(AppConfig):
    name = 'stripe_middleware'
