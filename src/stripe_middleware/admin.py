from django.contrib import admin

# Register your models here.
from stripe_middleware import models

admin.site.register(models.TransactionRequest)
admin.site.register(models.TransactionResponse)
admin.site.register(models.ClientCard)