# IVR task

This is an IVR task that simulates a middleware that receives a POST REQUEST, it connects to the Stripe API and
returns the result of the transaction.

# Prerequisites

To begin, you will need a database, a user and a password created in MySQL or Maria DB

If you do not have MySQL on your server you can follow this installation guide. It works on later versions than
ubuntu 14.04

Installation guide: https://www.digitalocean.com/community/tutorials/how-to-use-mysql-or-mariadb-with-your-django-application-on-ubuntu-14-04

# Installation

Download or clone the repository
Run the next line to install all the requirements

    pip install -r requirements.txt

create a file called .env in the repository base with the following information

    DEBUG=True
    IVR_DB_NAME=[your_db]
    IVR_DB_USER=[your_user]
    IVR_DB_PASS=[your_pass]
    IVR_TASK_HOST=[your_host] # can be localhost
    IVR_DB_PORT=[your_port] # normaly 3306
    IVR_STRIPE_PUB_KEY= [your_stripe_pub_key] # this is a test key: pk_test_WmR8J6gyYCG5C5XJPV4kvSVs00BVQ08TWn
    IVR_STRIPE_SECRET_KEY=[your_stripe_secret_key] # this is a secret key: sk_test_8xJxYBBCSLxlik83seMHHkxX00jbeTfbKz

Create the tables of the database

    python manage.py makemigrations
    python manage.py migrate

 Create a superuser

    python manage.py createsuperuser

 Run the development server

    python manage.py runserver

# Test

When the project is running, you can try send the next JSON

    {
        "cc_numb": "4242424242424242",
        "cvc": "123",
        "exp_date": "1220",
        "trans_id": "321654351687"
    }

# Author
* Fiorella Hérnandez Leal
